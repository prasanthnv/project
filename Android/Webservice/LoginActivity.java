package com.codemagos.threewheels;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codemagos.threewheels.Spstore.SharedPreferenceStore;
import com.codemagos.threewheels.Webservice.WebService;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity {
    TextView txt_signup;
    EditText txt_email, txt_password;
    Button btn_submit;
    String email, password;
    LinearLayout btn_driver, btn_passenger;
    SharedPreferenceStore spStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#33000000")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#55000000")));
        setContentView(R.layout.activity_login);
        spStore = new SharedPreferenceStore(getApplicationContext());
        txt_email = (EditText) findViewById(R.id.txt_email);
        txt_password = (EditText) findViewById(R.id.txt_password);
        txt_signup = (TextView) findViewById(R.id.txt_signup);
        btn_submit = (Button) findViewById(R.id.btn_login);


/*--------------------*/
        //adding text as html to get multi color in single textview
        String signuptext = "<font color=#ffffff>Dont have an account yet</font> <font color=#000000> <u><b>Create now</b></u></font>";
        txt_signup.setText(Html.fromHtml(signuptext));
/*-----------------*/
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = txt_email.getText().toString();
                password = txt_password.getText().toString();
                if (email.equals("") || password.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please fill all Fileds", Toast.LENGTH_SHORT).show();
                } else {
                    BackTask bb = new BackTask();
                    bb.execute();
                }
            }
        });

        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(LoginActivity.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
               // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_choose_user_type);
                dialog.show();
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                dialog.getWindow().setLayout(metrics.widthPixels, metrics.heightPixels);
                btn_driver = (LinearLayout) dialog.findViewById(R.id.btn_driver);
                btn_passenger = (LinearLayout) dialog.findViewById(R.id.btn_passenger);
                Intent i = new Intent();

                btn_driver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(),MapsActivity.class));
                    }
                });

                btn_passenger.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(),RegistrationActivity.class));
                    }
                });
            }
        });

    }


    protected class BackTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            Toast.makeText(getApplicationContext(), "Fetching data....", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {
            return WebService.checkLogin(email, password);
        }

        @Override
        protected void onPostExecute(String response) {
            Log.w("Server ->",response);
            try {
                JSONObject json = new JSONObject(response);
                if (json.getString("status").equals("ok")) {
                    Toast.makeText(getApplicationContext(), "YES", Toast.LENGTH_LONG).show();
                    JSONObject result = json.getJSONObject("response");
                    String user_id = result.getString("id");
                    String user_name = result.getString("name");
                    String user_avatar = result.getString("photo");
                    String user_type = result.getString("type");
                    System.out.println(user_id);
                    System.out.println(user_name);
                    spStore.setLogin(user_id, user_name, user_avatar,user_type);
                    if(user_type.equals("driver")) {
                        startActivity(new Intent(getApplicationContext(), DriverHomeActivity.class));
                    }else{
                        startActivity(new Intent(getApplicationContext(), DistanceFindActivity.class));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Username or Password Error\nPlease Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
