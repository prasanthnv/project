package com.codemagos.threewheels.Webservice;

import android.util.Log;

import com.codemagos.threewheels.Config.Configuration;
import com.codemagos.threewheels.Spstore.SharedPreferenceStore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Sree on 11-Mar-16.
 */
public class WebService {

    static String reg_url = Configuration.IP_ADDRESSS+"/webservice.php";

    public static String startWebService(String action_url, String data) {
        String responce = "";
        URL url;
        Log.w("-->", "In web service");
        try {
            Log.w("-->", "In web service Try");
            url = new URL(action_url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write(data);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
            String line = "";

            while ((line = bufferedReader.readLine()) != null) {
                responce += line;
            }
            bufferedReader.close();

            inputStream.close();
            httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.w("-->", "in web service First catch");
        } catch (ProtocolException e) {
            e.printStackTrace();
            Log.w("-->", "in web service Second catch");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.w("-->", "in web service Third catch");
        } catch (IOException e) {
            e.printStackTrace();
            Log.w("-->", "in web service Fourth catch");
        }
        return responce;
    }

    public static String checkLogin(String email, String password) {
        String data = "";
        try {
            data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("login_check", "UTF-8") + "&" +
                    URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&" +
                    URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return startWebService(reg_url, data);
    }

    public static String demo(String user_id, double latitude, double longitude) {
        String data = null;
        try {
            data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("update_location", "UTF-8") + "&" +
                    URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8") + "&" +
                    URLEncoder.encode("latitude", "UTF-8") + "=" + URLEncoder.encode("" + latitude, "UTF-8") + "&" +
                    URLEncoder.encode("longitude", "UTF-8") + "=" + URLEncoder.encode("" + longitude, "UTF-8");

            System.out.println(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return startWebService(reg_url, data);
    }
    public static String findDriver(String user_id, double latitude, double longitude) {
        String data = null;
        try {
            data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("find_driver", "UTF-8") + "&" +
                    URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8") + "&" +
                    URLEncoder.encode("latitude", "UTF-8") + "=" + URLEncoder.encode("" + latitude, "UTF-8") + "&" +
                    URLEncoder.encode("longitude", "UTF-8") + "=" + URLEncoder.encode("" + longitude, "UTF-8");

            System.out.println(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return startWebService(reg_url, data);
    }
    public static String driver_info(String id) {
        String data = null;
        try {
            data = URLEncoder.encode("action", "UTF-8") + "=" + URLEncoder.encode("driver_info", "UTF-8") + "&" +
                    URLEncoder.encode("id", "UTF-8") + "=" + URLEncoder.encode(id, "UTF-8") ;

            System.out.println(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return startWebService(reg_url, data);
    }


}
