package com.codemagos.threewheels.Spstore;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceStore {
	public final static String SHARED_PREFS = "PreferenceStore";
	private Editor edit;
	private SharedPreferences settings;
	public SharedPreferenceStore(Context context){
		settings = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
	}
	public void setLogin(String id,String name,String avatar,String type){
		edit = settings.edit();
		edit.putString("LOG_ID", id);
		edit.putString("LOG_NAME", name);
		edit.putString("LOG_AVATAR", avatar);
		edit.putString("LOG_TYPE", type);
		edit.commit();
	}

	public String getLoginID(){
		return settings.getString("LOG_ID","");
	}
	public String getName(){
		return settings.getString("LOG_NAME","");
	}
	public String getAvatar(){
		return settings.getString("LOG_AVATAR","");
	}
	public String getType(){
		return settings.getString("LOG_TYPE","");
	}
	public void removeLoginStatus(){
		edit = settings.edit();
		edit.putString("LOG_ID", "");
		edit.putString("LOG_NAME", "");
		edit.commit();
	}



}
