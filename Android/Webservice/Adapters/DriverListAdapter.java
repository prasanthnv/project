package com.codemagos.threewheels.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.codemagos.threewheels.R;
import com.codemagos.threewheels.Webservice.ImageParse;

/**
 * Created by Sree on 26-Mar-16.
 */
public class DriverListAdapter extends ArrayAdapter<String> {
    final Activity activity;
    final String[] names;
    final String[] ids;
    final String[] distance;
    final String[] phone;
    final String[] photo;


    public DriverListAdapter(Activity activity, String[] names,String[] ids,String[] distance,String[] phone,String[] photo) {
        super(activity.getApplicationContext(), R.layout.listview_driver_item,names);
        this.activity = activity;
        this.names = names;
        this.ids = ids;
        this.distance = distance;
        this.phone = phone;
        this.photo = photo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rawView = inflater.inflate(R.layout.listview_driver_item, null);
        TextView txt_name = (TextView) rawView.findViewById(R.id.txt_driver_name);
        ImageView img_photo = (ImageView) rawView.findViewById(R.id.img_driver_avatar);
        txt_name.setText(names[position]+"  ("+distance[position]+" away)  ");
        try {
            img_photo.setImageBitmap(ImageParse.base64ToImage(photo[position]));
        } catch (Exception e) {
            e.printStackTrace();
        }
        img_photo.setScaleType(ImageView.ScaleType.FIT_XY);
        return rawView;
    }
}
