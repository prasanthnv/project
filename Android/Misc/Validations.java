package in.ambittech.mybook.Misc;

/**
 * Created by Sree on 30-Oct-15.
 */
public class Validations {
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
