package in.ambittech.mybook.Misc;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import in.ambittech.mybook.Misc.ReminderNotification;

/**
 * Code with love by Sree on 23-Oct-15.
 * For Receiving Alarm manger Broadcasts
 *
 */
public class AlertReceiverBroadCast extends BroadcastReceiver {
    ReminderNotification reminderNotification;
    @Override

    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "In on recive", Toast.LENGTH_LONG).show();
        reminderNotification = new ReminderNotification(context);
        reminderNotification.showNotification("Mybook Notification","You have one Reminder","Click to open Application");
    }
}
