package in.ambittech.mybook.Misc;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import in.ambittech.mybook.MainActivity;
import in.ambittech.mybook.R;
/**
 * Code with love by Sree on 23-Oct-15.
 * For Creating Notfications and Alarms in the given Date time...
 */
public class ReminderNotification {
    NotificationManager notificationManager;
    Boolean isNotificationActive = false;
    public static int NOTIFY_ID = 100;
   Context context;

    public ReminderNotification(Context context) {
        this.context = context;
    }

    public void showNotification(String ticker,String title,String message) {
        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(message)
                .setTicker(ticker)
                .setDefaults(NotificationCompat.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(intent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notifiBuilder.setContentIntent(pendingIntent);
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = notifiBuilder.build();
        notificationManager.notify(NOTIFY_ID, notification);
        isNotificationActive = true;
    }

    public void stopNotification(){
        if (isNotificationActive) {
            notificationManager.cancel(NOTIFY_ID);
        }else{
            Toast.makeText(context,"No Notification is Running !",Toast.LENGTH_LONG).show();
        }
    }

    public  void setAlarm(int date, int month, int year, int hour, int min, int sec){
        Calendar cal = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
//cal.add(Calendar.SECOND, 10);

        cal.set(Calendar.DATE,date);  //1-31
        cal.set(Calendar.MONTH,month);  //first month is 0!!! January is zero!!!
        cal.set(Calendar.YEAR, year);//year...

        cal.set(Calendar.HOUR_OF_DAY, hour);  //HOUR
        cal.set(Calendar.MINUTE, min);       //MIN
        cal.set(Calendar.SECOND, sec);       //SEC


        Long alertTime = new GregorianCalendar().getTimeInMillis() + 5 * 1000;
        Intent alertIntent = new Intent(context, AlertReceiverBroadCast.class);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                cal.getTimeInMillis(), PendingIntent.getBroadcast(context, 1,
                        alertIntent, PendingIntent.FLAG_UPDATE_CURRENT));
    }
}
