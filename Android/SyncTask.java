package in.ambittech.mybook;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import in.ambittech.mybook.Alerts.Alerts;
import in.ambittech.mybook.Database.DbHelper;
import in.ambittech.mybook.Misc.ConnectionDetector;
import in.ambittech.mybook.Spstore.SharedPreferenceStore;
import in.ambittech.mybook.Webservice.WebService;

/**
 * Created by Sree on 07-Oct-15.
 */
public class SyncTask {
    Activity activity;
    static SharedPreferenceStore spStore;
    String sync_response;
    static DbHelper dbHelper;
    static Cursor account_cursor, payment_cursor, account_pay_cursor;
    static SQLiteDatabase sqLiteDatabase;
    ConnectionDetector connectionDetector;
   Boolean isHasConnection = false;
    public SyncTask(Activity activity) {
        this.activity = activity;
        spStore = new SharedPreferenceStore(activity.getApplicationContext());
        dbHelper = new DbHelper(activity.getApplicationContext());
        connectionDetector = new ConnectionDetector(activity.getBaseContext());
        isHasConnection = connectionDetector.isConnectingToInternet();
    }

    public void syncAtoS() {
        // sync android to server
        if(isHasConnection){
            sqLiteDatabase = dbHelper.getReadableDatabase();
            account_cursor = dbHelper.getAccount(sqLiteDatabase, spStore.getLoginID());
            payment_cursor = dbHelper.getPayment("", sqLiteDatabase, spStore.getLoginID());
            String accountData = cur2Json(account_cursor).toString();
            String paymentData = cur2Json(payment_cursor).toString();
            if (account_cursor.moveToFirst()) {
                androidToServerSync bt = new androidToServerSync();
                bt.execute(accountData, paymentData);
            }else{

            }
            account_cursor.close();
            payment_cursor.close();
        }else{
            Toast.makeText(activity.getBaseContext(),"Network not Available",Toast.LENGTH_LONG).show();
        }

    }

    public void syncStoA() {
        // syncs server to android

        if(isHasConnection) {
            serverToAndroidSync bt = new serverToAndroidSync();
            bt.execute("");
        }else{
            Toast.makeText(activity.getBaseContext(),"Network not Available",Toast.LENGTH_LONG).show();
        }

    }

    protected class androidToServerSync extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            Alerts.progressDialog(activity, "Syncing Plase Wait.....");
        }

        @Override
        protected String doInBackground(String... params) {
            String id = spStore.getLoginID();
            return WebService.syncAtoS(id, params[0], params[1]);
        }

        @Override
        protected void onPostExecute(String response) {

            sync_response = response.trim();
            if (!sync_response.equals("error")) {
                Log.e("-->", sync_response);

            } else {

            }
            Alerts.progressDialogClose();
        }
    }

    protected class serverToAndroidSync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            Alerts.progressDialog(activity, "Syncing Plase Wait.....");
        }

        @Override
        protected String doInBackground(String... params) {
            String id = spStore.getLoginID();
            return WebService.syncStoA(id);
        }

        @Override
        protected void onPostExecute(String response) {

            sync_response = response.trim();
            SQLiteDatabase sql_read = SyncTask.dbHelper.getReadableDatabase();
            SQLiteDatabase sql_write = SyncTask.dbHelper.getWritableDatabase();
            Cursor payment_cursor, account_cursor;
            payment_cursor = SyncTask.dbHelper.getPayment("", sql_read, SyncTask.spStore.getLoginID());
            account_cursor = SyncTask.dbHelper.getAccount(sql_read, SyncTask.spStore.getLoginID());
            Log.e("cursor", "Payment -> " + payment_cursor.getColumnCount());
            Log.e("cursor", "Account -> " + account_cursor.getColumnCount());
            if (!sync_response.equals("error")) {
                Log.e("-->", sync_response);
                try {
                    JSONObject jsonobj = new JSONObject(sync_response);
                    JSONArray paymentsArray = jsonobj.getJSONArray("payment");
                    JSONArray accountsArray = jsonobj.getJSONArray("account");
                    Log.v("JSON", "" + paymentsArray.length());
                    if (!account_cursor.moveToFirst()) {
                        Log.e("cursor", "Accounts is empty");
                        for (int i = 0; i < accountsArray.length(); i++) {
                            String id = accountsArray.getJSONObject(i).getString("account_id");
                            String name = accountsArray.getJSONObject(i).getString("name");
                            String email = accountsArray.getJSONObject(i).getString("email");
                            String phone = accountsArray.getJSONObject(i).getString("email");
                            String description = accountsArray.getJSONObject(i).getString("email");
                            SyncTask.dbHelper.addAccountBySync(id, SyncTask.spStore.getLoginID(), name, phone, email, description, sql_write);
                        }
                    }
                    if (!payment_cursor.moveToFirst()) {
                        Log.e("cursor", paymentsArray.toString());
                        for (int j = 0; j < paymentsArray.length(); j++) {
                            String id = paymentsArray.getJSONObject(j).getString("payment_id");
                            String account_id = paymentsArray.getJSONObject(j).getString("account_id");
                            String amount = paymentsArray.getJSONObject(j).getString("amount");
                            String type = paymentsArray.getJSONObject(j).getString("type");
                            String note = paymentsArray.getJSONObject(j).getString("note");
                            String date = paymentsArray.getJSONObject(j).getString("date");
                            SyncTask.dbHelper.addPaymentBySync(id, SyncTask.spStore.getLoginID(), account_id, type, amount, note, date, sql_read);
                            Log.e("cursor id", id);
                        }
                    }
                    activity.startActivity(new Intent(activity.getBaseContext(),
                            MainActivity.class));
                    activity.finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

            }

            Alerts.progressDialogClose();
        }
    }

    public JSONArray cur2Json(Cursor cursor) {
        JSONArray resultSet = new JSONArray();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            int totalColumn = cursor.getColumnCount();
            JSONObject rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        rowObject.put(cursor.getColumnName(i),
                                cursor.getString(i));
                    } catch (Exception e) {
                        Log.d("Json Conversion", e.getMessage());
                    }
                }
            }
            resultSet.put(rowObject);
            cursor.moveToNext();
        }
  return resultSet;

    }

}
