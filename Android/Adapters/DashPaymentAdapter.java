package in.ambittech.mybook.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.ambittech.mybook.R;

/**
 * Created by Sree on 29-Sep-15.
 */
public class DashPaymentAdapter extends ArrayAdapter {
    ArrayList names;
    ArrayList amounts;
    public DashPaymentAdapter(Context context, ArrayList names, ArrayList amounts) {
        super(context, R.layout.listview_debit_row, names);
        this.names = names;
        this.amounts = amounts;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.listview_debit_row, parent, false);
        TextView txt_name = (TextView) customView.findViewById(R.id.list_txt_d_name);
        TextView txt_amount = (TextView) customView.findViewById(R.id.list_txt_amount);
        txt_name.setText(names.get(position).toString()); // name
        txt_amount.setText(amounts.get(position).toString()+" ₹");

        return customView;
    }
}
