package in.ambittech.mybook.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.media.AudioTrack;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import in.ambittech.mybook.R;

/**
 * Created by Sree on 29-Sep-15.
 */
public class DebitCreditAdapter extends ArrayAdapter {
    ArrayList names;
    ArrayList amounts;
    ArrayList date;
    ArrayList type;
    public DebitCreditAdapter(Context context, ArrayList names, ArrayList amounts,ArrayList date,ArrayList type) {
        super(context, R.layout.listview_debit_row, names);
        this.names = names;
        this.amounts = amounts;
        this.date = date;
        this.type = type;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.listview_payment_row, parent, false);
        String singlename = names.get(position).toString();
        String singleamount = amounts.get(position).toString();
        TextView txt_name = (TextView) customView.findViewById(R.id.list_txt_d_name);
        TextView txt_date = (TextView) customView.findViewById(R.id.list_txt_d_date);
        TextView txt_type = (TextView) customView.findViewById(R.id.list_txt_d_type);
        TextView txt_amount = (TextView) customView.findViewById(R.id.list_txt_amount);
        txt_name.setText(names.get(position).toString()); // name
        txt_date.setTag(date.get(position).toString()); // date
        txt_type.setTag(type.get(position).toString()); // type
        txt_amount.setText("₹ "+amounts.get(position).toString()); // amount
        return customView;
    }
}
