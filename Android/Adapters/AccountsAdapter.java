package in.ambittech.mybook.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import in.ambittech.mybook.R;

/**
 * Created by Sree on 29-Sep-15.
 */
public class AccountsAdapter extends ArrayAdapter {
    ArrayList names;
    ArrayList ids;

    public AccountsAdapter(Context context, ArrayList names, ArrayList ids) {
        super(context, R.layout.listview_account_row, names);
        this.names = names;
        this.ids = ids;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.listview_account_row, parent, false);
        String singlename = names.get(position).toString();
        TextView txt_name = (TextView) customView.findViewById(R.id.txt_account_name);
        Button btn = (Button) customView.findViewById(R.id.btn_account_icon_1);
        txt_name.setText(names.get(position).toString()); // name
        txt_name.setTag(ids.get(position).toString()); // id
        String colors[] = {"#EC407A", "#7B1FA2", "#303F9F", "#512DA8", "#0097A7", "#689F38", "#FFA000", "#303F9F"};
        String randColor = colors[new Random().nextInt(colors.length)];
        btn.setBackgroundColor(Color.parseColor(randColor.toString()));
        btn.setText(singlename.substring(0, 1));
        return customView;
    }
}
