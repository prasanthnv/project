package in.ambittech.mybook.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import in.ambittech.mybook.Fragments.AccountFragment;
import in.ambittech.mybook.Fragments.DashboardFragment;
import in.ambittech.mybook.Fragments.PaymentFragment;
import in.ambittech.mybook.Fragments.SettingsFragment;

/**
 * Created by Sree on 28-Sep-15.
 */
public class FragmentPageAdapter  extends FragmentPagerAdapter{

    public FragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new DashboardFragment();
            case 1:
                return new PaymentFragment();
            case 2:
                return new AccountFragment();
            case 3:
                return new SettingsFragment();
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
