package in.ambittech.mybook.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.ambittech.mybook.R;

/**
 * Created by Sree on 29-Sep-15.
 */
public class AccountPaymentAdapter extends ArrayAdapter {

    ArrayList amounts;
    ArrayList date;
    ArrayList type;
    public AccountPaymentAdapter(Context context, ArrayList amounts, ArrayList date, ArrayList type) {
        super(context, R.layout.listview_account_payment_row,date);
        this.amounts = amounts;
        this.date = date;
        this.type = type;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.listview_account_payment_row, parent, false);
        TextView txt_amount = (TextView) customView.findViewById(R.id.list_txt_d_amount);
        TextView txt_date = (TextView) customView.findViewById(R.id.list_txt_d_date);
        TextView txt_type = (TextView) customView.findViewById(R.id.list_txt_d_type);
        txt_amount.setText(amounts.get(position).toString()+" ₹"); //amount
       txt_date.setText(date.get(position).toString());// date
        txt_type.setText(type.get(position).toString());// type

        return customView;
    }
}
