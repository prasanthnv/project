package com.codemagos.dbdemo.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by prasanth on 5/3/17.
 */

public class DbHelper extends SQLiteOpenHelper {
    public DbHelper(Context context) {
        super(context, "DB_DEMO", null, 1);
        Log.w("Database","Data Opened");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS user(name TEXT,email TEXT,phone TEXT);");
        Log.w("Database","Table Created or table opend");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addUser(SQLiteDatabase db,String name,String email,String phone){
        db.execSQL("INSERT INTO user VALUES('"+name+"','"+email+"','"+phone+"');");
        Log.w("Database","Data inserted");
    }
    public Cursor getUser(SQLiteDatabase db){
        Cursor resultSet = db.rawQuery("SELECT * FROM user",null);
        return resultSet;
    }






}
