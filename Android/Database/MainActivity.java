package com.codemagos.dbdemo;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.codemagos.dbdemo.Database.DbHelper;

public class MainActivity extends AppCompatActivity {
    DbHelper dbHelper;
    SQLiteDatabase sql;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DbHelper(getApplicationContext());
        Button btn = (Button)findViewById(R.id.btn);
        sql = dbHelper.getWritableDatabase();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.addUser(sql,"prasanth","prasanth@codemagos.com","9949658754");

            }
        });

        Button btn_get = (Button) findViewById(R.id.btn_get);
        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Cursor resultSet =  dbHelper.getUser(sql);
                if(!resultSet.moveToFirst()){
                    Toast.makeText(getApplicationContext(),"Empty Table",Toast.LENGTH_LONG).show();
                }else{
                    while (resultSet.moveToNext()){
                        Log.e("user name",resultSet.getString(0));
                    }
                }

            }
        });

    }
}
