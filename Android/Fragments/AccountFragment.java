package in.ambittech.mybook.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.ambittech.mybook.AccountPayDetailsActivity;
import in.ambittech.mybook.Adapters.AccountsAdapter;
import in.ambittech.mybook.Database.DbHelper;
import in.ambittech.mybook.R;
import in.ambittech.mybook.Spstore.SharedPreferenceStore;

/**
 * Created by Sree on 28-Sep-15.
 */
public class AccountFragment extends Fragment {
    ListView accountsList;
    Button btnAdd, btnImport;
    SharedPreferenceStore spStore;
    Dialog accountAddDialog;
    String acc_id = "", acc_name, acc_email, acc_mobile, acc_description;

    final static int PICK_CONTACT = 100;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_accounts, container, false);
        btnAdd = (Button) rootView.findViewById(R.id.btn_add_contact);
        btnImport = (Button) rootView.findViewById(R.id.btn_import_contact);
        accountsList = (ListView) rootView.findViewById(R.id.listview_accounts);
        dbHelper = new DbHelper(getContext());
        spStore = new SharedPreferenceStore(getContext());
        showDataInList();


        accountsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView accountName = (TextView) view.findViewById(R.id.txt_account_name);
                final String selected_id = accountName.getTag().toString();
                LayoutInflater accountManageDialogViewInflator = getActivity().getLayoutInflater();
                final View accountManageDialogView = accountManageDialogViewInflator.inflate(R.layout.dialog_account_manage_item, null);
                AlertDialog.Builder accountManageDialogBuilder = new AlertDialog.Builder(getActivity());
                accountManageDialogBuilder.setView(accountManageDialogView);
                final Dialog accountManageDialog = accountManageDialogBuilder.create();
                accountManageDialog.show();
                Button btnAccountDelete = (Button) accountManageDialogView.findViewById(R.id.btn_delete_account);
                Button btnAccountUpdate = (Button) accountManageDialogView.findViewById(R.id.btn_update_account);
                Button btnViewTrans = (Button) accountManageDialogView.findViewById(R.id.btn_view_transactions);
                btnAccountDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteAccount(selected_id);
                        accountManageDialog.cancel();
                    }
                });
                btnViewTrans.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                     Intent intent = new Intent(getContext(), AccountPayDetailsActivity.class);
                        intent.putExtra("id",selected_id);
                        startActivity(intent);
                    }
                });
                btnAccountUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sqLiteDatabase = dbHelper.getReadableDatabase();
                        cursor = dbHelper.getAccount(sqLiteDatabase, selected_id,spStore.getLoginID());
                        if (cursor.moveToFirst()) {
                            Toast.makeText(getContext(), cursor.getString(cursor.getColumnIndex("name")), Toast.LENGTH_SHORT).show();
                            acc_id = selected_id;
                            acc_name = cursor.getString(cursor.getColumnIndex("name"));
                            acc_mobile =  cursor.getString(cursor.getColumnIndex("mobile"));
                            acc_email =  cursor.getString(cursor.getColumnIndex("email"));
                            acc_description =  cursor.getString(cursor.getColumnIndex("description"));
                        }

                        accountAddDialogShow();
                        accountManageDialog.cancel();
                    }
                });
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountAddDialogShow();
            }
        });
        btnImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });


        return rootView;

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {

        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String has_phone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (Integer.parseInt(has_phone) > 0) {
                            // Query phone here. Covered next
                            Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                            while (phones.moveToNext()) {
                                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                              /*  Log.i("Number", phoneNumber);*/
                                acc_mobile = phoneNumber;
                            }
                            phones.close();
                        }
                        // TODO Whatever you want to do with the selected contact name.
                        acc_name = name;
                        accountAddDialogShow();
                        //  Toast.makeText(getActivity(), "Name : " + contact_name + "Phone : " + contact_phone, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public void accountAddDialogShow(){
        LayoutInflater dialogInflator = getActivity().getLayoutInflater();
        final View dialogView = dialogInflator.inflate(R.layout.dialog_add_account, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText input_name = (EditText) dialogView.findViewById(R.id.input_account_name);
        final EditText input_phone = (EditText) dialogView.findViewById(R.id.input_account_phone);
        final EditText input_email = (EditText) dialogView.findViewById(R.id.input_account_email);
        final EditText input_description = (EditText) dialogView.findViewById(R.id.input_account_description);
        input_name.setText(acc_name);
        input_email.setText(acc_email);
        input_phone.setText(acc_mobile);
        input_description.setText(acc_description);
        builder.setView(dialogView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                        /* For creating account */
                acc_name = input_name.getText().toString();
                acc_email = input_email.getText().toString();
                acc_mobile = input_phone.getText().toString();
                acc_description = input_description.getText().toString();
                addAccount();
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                        /* To cancel action */

            }
        });
        // to create dialog from builder.
        accountAddDialog = builder.create();
        accountAddDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {                    //
                Button positiveButton = ((AlertDialog) dialog)
                        .getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setBackgroundColor(Color.parseColor("#f5f5f5"));
                positiveButton.setTextColor(Color.parseColor("#262626"));
                Button negativeButton = ((AlertDialog) dialog)
                        .getButton(AlertDialog.BUTTON_NEGATIVE);
                negativeButton.setBackgroundColor(Color.parseColor("#f5f5f5"));
                negativeButton.setTextColor(Color.parseColor("#262626"));
            }
        });
        accountAddDialog.show();
    }
    public void addAccount() {
        //todo Inserting in to db
        sqLiteDatabase = dbHelper.getWritableDatabase();

        dbHelper.addAccount(acc_id,spStore.getLoginID(),acc_name, acc_mobile, acc_email, acc_description, sqLiteDatabase);
        Toast.makeText(getContext(), "Contact added !!", Toast.LENGTH_SHORT).show();
        acc_name = acc_mobile = acc_email = acc_description = "";
        showDataInList();
    }

    public void deleteAccount(String id) {
        //todo Inserting in to db
        sqLiteDatabase = dbHelper.getWritableDatabase();
        dbHelper.deleteAccount(id, sqLiteDatabase);
        Toast.makeText(getContext(), "Contact Deleted !!", Toast.LENGTH_SHORT).show();
        showDataInList();
    }

    public void updateAccount(String id) {

    }

    public void showDataInList() {
        Log.e("bang", "refreshed");
        // Getting data from Database and Setting it to the listview
        /*---------db-------------*/
        sqLiteDatabase = dbHelper.getReadableDatabase();
        cursor = dbHelper.getAccount(sqLiteDatabase,spStore.getLoginID());
        /*-----------------------*/
        ArrayList names = new ArrayList();
        ArrayList ids = new ArrayList();

        if (cursor.moveToFirst()) {
            do {
                String id, name, mob, email, desc;
                id = cursor.getString(0);
                name = cursor.getString(2);
                mob = cursor.getString(3);
                email = cursor.getString(4);
                desc = cursor.getString(5);
                ids.add(id);
                names.add(name);
            } while (cursor.moveToNext());
            Log.e("ss", names.toString());

        }
        ListAdapter adapter = new AccountsAdapter(this.getActivity(), names, ids);
        accountsList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        showDataInList();
        super.onResume();
    }
 /*----------------------------*/
}
