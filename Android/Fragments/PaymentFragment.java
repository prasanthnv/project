package in.ambittech.mybook.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import in.ambittech.mybook.Adapters.DebitCreditAdapter;
import in.ambittech.mybook.Adapters.PaymentAdapter;
import in.ambittech.mybook.AddPaymentActivity;
import in.ambittech.mybook.Database.DbHelper;
import in.ambittech.mybook.R;
import in.ambittech.mybook.Spstore.SharedPreferenceStore;

/**
 * Created by Sree on 28-Sep-15.
 */
public class PaymentFragment extends Fragment {
    ListView listview_payment;
    DbHelper dbHelper;
    SharedPreferenceStore spStore;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor_payment;
    String log_id;
    Intent intent;
    ArrayList names,ids,amounts,payment_ids,payment_date,payment_type;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment, container, false);
        spStore = new SharedPreferenceStore(getContext());
        listview_payment = (ListView) rootView.findViewById(R.id.listview_payment);
        log_id = spStore.getLoginID();

        // Getting data from Database and Setting it to the listview

        /*---------db-------------*/

showList();


        /*-----------------------*/
        listview_payment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder accountManageDialogBuilder = new AlertDialog.Builder(getActivity());
                LayoutInflater accountManageDialogViewInflator = getActivity().getLayoutInflater();
                final View accountManageDialogView = accountManageDialogViewInflator.inflate(R.layout.dialog_manage_item, null);
                accountManageDialogBuilder.setView(accountManageDialogView);
                final Dialog accountManageDialog = accountManageDialogBuilder.create();
                accountManageDialog.show();
                Button btnAccountDelete = (Button) accountManageDialogView.findViewById(R.id.btn_delete_account);
                Button btnAccountUpdate = (Button) accountManageDialogView.findViewById(R.id.btn_update_account);
                TextView dilaogHeader = (TextView) accountManageDialogView.findViewById(R.id.txt_manage_dialog_title);
                dilaogHeader.setText("Manage Payment");
                btnAccountDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       DbHelper deletedbHelper = new DbHelper(getContext());
                        SQLiteDatabase deletesqLiteDatabase = deletedbHelper.getWritableDatabase();
                        dbHelper.deletePayment(payment_ids.get(position).toString(),sqLiteDatabase);
                        cursor_payment = dbHelper.getPayment("", deletesqLiteDatabase, payment_ids.get(position).toString());
                        showList();
                        accountManageDialog.cancel();
                    }
                });
                btnAccountUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                intent = new Intent(getContext(), AddPaymentActivity.class);
                        intent.putExtra("payment_id",payment_ids.get(position).toString());
                startActivity(intent);
                        accountManageDialog.cancel();
                    }
                });
                /*
                Intent intent;
                intent = new Intent(getContext(), AccountPayDetailsActivity.class);
                startActivity(intent);
                */
            }
        });
        return rootView;
    }

    public void showList() {
        dbHelper = new DbHelper(getContext());
        sqLiteDatabase = dbHelper.getReadableDatabase();
        cursor_payment = dbHelper.getPayment("", sqLiteDatabase, spStore.getLoginID());
        names = new ArrayList();
        ids = new ArrayList();
        amounts = new ArrayList();
        payment_ids = new ArrayList();
        payment_type = new ArrayList();
        payment_date = new ArrayList();
        Cursor account_cursor;

        if (cursor_payment.moveToFirst()) {
            do {
                Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(cursor_payment));
                String id, account_id, account_name, amount, date,type;
                id = cursor_payment.getString(0);
                payment_ids.add(id);
                account_id = cursor_payment.getString(2);
                account_cursor = dbHelper.getAccount(sqLiteDatabase, account_id, log_id);
                if (account_cursor.moveToFirst()) {
                    account_name = account_cursor.getString(2);
                } else {
                    account_name = "";
                }
                amount = cursor_payment.getString(4);
                date = cursor_payment.getString(6);
                type = cursor_payment.getString(3);

                ids.add(account_id);
                names.add(account_name);
                amounts.add(amount);
                payment_date.add(date);
                payment_type.add(type);
            } while (cursor_payment.moveToNext());

        }
        ListAdapter adapter = new PaymentAdapter(this.getActivity(), names, amounts, payment_date, payment_type);
        listview_payment.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        showList();
        super.onResume();
    }

}
