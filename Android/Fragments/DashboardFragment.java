package in.ambittech.mybook.Fragments;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import in.ambittech.mybook.Adapters.AccountsAdapter;
import in.ambittech.mybook.Adapters.DashPaymentAdapter;
import in.ambittech.mybook.Adapters.DebitCreditAdapter;
import in.ambittech.mybook.Database.DbHelper;
import in.ambittech.mybook.R;
import in.ambittech.mybook.Spstore.SharedPreferenceStore;

/**
 * Created by Sree on 28-Sep-15.
 */
public class DashboardFragment extends Fragment {
    ListView listview_credit, listview_debit;
    DbHelper dbHelper;
    SharedPreferenceStore spStore;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor_debit, cursor_credit;
    Double debit = Double.parseDouble("0"),credit = Double.parseDouble("0");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        listview_credit = (ListView) rootView.findViewById(R.id.listview_credit);
        listview_debit = (ListView) rootView.findViewById(R.id.listview_debit);
        spStore = new SharedPreferenceStore(getContext());

        // Getting data from Database and Setting it to the listview
        /*---------db-------------*/
        dbHelper = new DbHelper(getContext());
        sqLiteDatabase = dbHelper.getReadableDatabase();

        cursor_debit = dbHelper.getPayment("debit", sqLiteDatabase, spStore.getLoginID());
        cursor_credit = dbHelper.getPayment("credit", sqLiteDatabase, spStore.getLoginID());
        showList(cursor_credit, listview_credit);
        showList(cursor_debit, listview_debit);
        /*-----------------------*/
        return rootView;
    }

    @Override
    public void onResume() {
        showList(cursor_credit, listview_credit);
        showList(cursor_debit, listview_debit);
        super.onResume();
    }

    @Override
    public void onStart() {
        showList(cursor_credit, listview_credit);
        showList(cursor_debit, listview_debit);
        super.onStart();
    }

    public void showList(Cursor cursor, ListView listView) {
        Cursor account_cursor;
        ArrayList names = new ArrayList();
        ArrayList dates = new ArrayList();
        ArrayList amounts = new ArrayList();
        ArrayList payment_id = new ArrayList();
        ArrayList types = new ArrayList();
        if (cursor.moveToFirst()) {

            do {
                String id, account_id, account_name, amount, date;
                payment_id.add(cursor.getString(0));
                date = cursor.getString(6);
                account_id = cursor.getString(2); // account id
                account_cursor = dbHelper.getAccount(sqLiteDatabase, account_id, spStore.getLoginID());

                if (account_cursor.moveToFirst()) {
                    account_name = account_cursor.getString(2);
                } else {
                    account_name = "45";
                }
                amount = cursor.getString(4);
                date = cursor.getString(5);
                dates.add(date);
                types.add(cursor.getString(3));
                names.add(account_name);
                amounts.add(amount);
                if(types.equals("debit")){
                    debit = debit+Double.parseDouble(amount);
                }else{
                    Log.e("Result","No");
                }
            } while (cursor.moveToNext());
         Log.e("Result",Double.toString(debit));
            ListAdapter adapter = new DashPaymentAdapter(this.getActivity(), names, amounts);
            listView.setAdapter(adapter);

        } else {
            // todo contact list empty

        }
    }
}
