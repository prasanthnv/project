package in.ambittech.mybook.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import in.ambittech.mybook.Adapters.DebitCreditAdapter;
import in.ambittech.mybook.Alerts.Alerts;
import in.ambittech.mybook.Database.DbHelper;
import in.ambittech.mybook.LoginActivity;
import in.ambittech.mybook.MainActivity;
import in.ambittech.mybook.PDFActivity;
import in.ambittech.mybook.R;
import in.ambittech.mybook.Spstore.SharedPreferenceStore;
import in.ambittech.mybook.SyncTask;
import in.ambittech.mybook.Webservice.WebService;

import static in.ambittech.mybook.R.id.input_change_pwd_curent_password;
import static in.ambittech.mybook.R.id.input_change_pwd_new_password;
import static in.ambittech.mybook.R.id.input_change_pwd_re_password;

/**
 * Created by Sree on 28-Sep-15.
 */
public class SettingsFragment extends Fragment {
    Button btn_sync, btn_logout, btn_print, btn_change;
    SharedPreferenceStore spStore;
    Intent printIntent;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;
    File myFile;
    Calendar myCalendar;
    EditText input_print_account, input_print_date_from, input_print_date_to;
    String print_account = "", print_date_from = "", print_date_to = "";
    String pass_change_response;
    String pass_change_cur_pass, pass_change_new_pass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        btn_sync = (Button) rootView.findViewById(R.id.btn_settings_sync);
        btn_logout = (Button) rootView.findViewById(R.id.btn_settings_logout);
        btn_change = (Button) rootView.findViewById(R.id.btn_settings_change_pwd);
        btn_print = (Button) rootView.findViewById(R.id.btn_settings_print);
        spStore = new SharedPreferenceStore(getContext());
        myCalendar = Calendar.getInstance();
        dbHelper = new DbHelper(getContext());
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Are You Sure")
                        .setMessage("Are you sure to Logout ?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                spStore.removeLoginStatus();
                                startActivity(new Intent(getActivity(), LoginActivity.class));
                                getActivity().finish();
                            }})
                        .show();

            }
        });
        btn_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncTask sync = new SyncTask(getActivity());
                sync.syncAtoS();
            }
        });
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printIntent = new Intent(getContext(), PDFActivity.class);
                // custom dialog
                LayoutInflater p_options_inflater = getActivity().getLayoutInflater();
                final View p_option_dialogView = p_options_inflater.inflate(R.layout.dialog_print_options, null);
                AlertDialog.Builder p_options_builder = new AlertDialog.Builder(getActivity());
                p_options_builder.setView(p_option_dialogView);
                Dialog p_options_dialog = p_options_builder.create();
                input_print_account = (EditText) p_option_dialogView.findViewById(R.id.input_print_account);
                input_print_date_from = (EditText) p_option_dialogView.findViewById(R.id.input_print_date_from);
                input_print_date_to = (EditText) p_option_dialogView.findViewById(R.id.input_print_date_to);
                Button print = (Button) p_option_dialogView.findViewById(R.id.btn_print);

                input_print_date_from.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(getActivity(), fromDateListener, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });
                input_print_date_to.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        new DatePickerDialog(getActivity(), toDateListener, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                });

                print.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Alerts.progressDialog(getContext(), "Building PDF....");
                        try {
                            createPdf();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (DocumentException e) {
                            e.printStackTrace();
                        }

                    }
                });
                final AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
                builderSingle.setIcon(R.drawable.custom_shape_border_bg);
                builderSingle.setTitle("Select One Name:-");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.select_dialog_singlechoice);

/*---------db-------------*/
                sqLiteDatabase = dbHelper.getReadableDatabase();
                cursor = dbHelper.getAccount(sqLiteDatabase,spStore.getLoginID());
        /*-----------------------*/
                final ArrayList names = new ArrayList();
                final ArrayList ids = new ArrayList();

                if (cursor.moveToFirst()) {
                    do {
                        String id, name, mob, email, desc;
                        id = cursor.getString(0);
                        name = cursor.getString(2);
                        names.add(name);
                        ids.add(id);
                        arrayAdapter.add(name);
                    } while (cursor.moveToNext());
                } else {

                }
                builderSingle.setNegativeButton(
                        "cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                builderSingle.setAdapter(
                        arrayAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String strName = names.get(which).toString();
                                print_account = ids.get(which).toString();
                                input_print_account.setText(strName);
                        /*AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                AddPaymentActivity.this);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected Item is");
                        builderInner.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builderInner.show();
 */
                            }
                        });

                input_print_account.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builderSingle.show();
                    }
                });
                p_options_dialog.show();
            }
        });

        btn_change.setOnClickListener(new View.OnClickListener() {
            Dialog pass_change_dialog;
            EditText input_cur_pass, input_new_pass, input_re_pass;

            @Override
            public void onClick(View v) {
                LayoutInflater pass_change_inflator = getActivity().getLayoutInflater();
                final View pass_change_view = pass_change_inflator.inflate(R.layout.dialog_change_password, null);
                AlertDialog.Builder pass_change_dialog_builder = new AlertDialog.Builder(getActivity());
                pass_change_dialog_builder.setView(pass_change_view);
                input_cur_pass = (EditText) pass_change_view.findViewById(input_change_pwd_curent_password);
                input_new_pass = (EditText) pass_change_view.findViewById(input_change_pwd_new_password);
                input_re_pass = (EditText) pass_change_view.findViewById(input_change_pwd_re_password);
                pass_change_dialog_builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        input_cur_pass.setText("");
                        input_new_pass.setText("");
                        input_re_pass.setText("");
                        pass_change_dialog.cancel();
                    }
                }).setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String cur_pass = input_cur_pass.getText().toString();
                        String new_pass = input_new_pass.getText().toString();
                        String re_pass = input_re_pass.getText().toString();
                        if (new_pass.equals(re_pass)) {

                            BackTask bt = new BackTask();
                            bt.execute(cur_pass, new_pass);
                        } else {
                            Toast.makeText(getContext(), "Password's are not Matching", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                pass_change_dialog = pass_change_dialog_builder.create();
                pass_change_dialog.show();
            }
        });
        return rootView;
    }

    final DatePickerDialog.OnDateSetListener fromDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String showDate = dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year;
            String date = year + "-"
                    + (monthOfYear + 1) + "-" + dayOfMonth;
            print_date_from = date;
            input_print_date_from.setText(showDate);

        }

    };
    final DatePickerDialog.OnDateSetListener toDateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String showDate = dayOfMonth + "-"
                    + (monthOfYear + 1) + "-" + year;
            String date = year + "-"
                    + (monthOfYear + 1) + "-" + dayOfMonth;
            print_date_to = date;
            input_print_date_to.setText(showDate);
        }

    };


    private void createPdf() throws FileNotFoundException, DocumentException {

        String FILE_NAME = "MyBook.pdf";
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File file = new File(path, FILE_NAME);
        path.mkdirs();

        //Create time stamp
        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

        myFile = file;

        OutputStream output = new FileOutputStream(myFile);

        //Step 1
        Document document = new Document();

        //Step 2
        PdfWriter.getInstance(document, output);

        //Step 3
        document.open();

        PdfPTable table = new PdfPTable(4); // 3 columns.

        PdfPCell table_head_1 = new PdfPCell(new Paragraph("Date"));
        PdfPCell table_head_2 = new PdfPCell(new Paragraph("Account Name"));
        PdfPCell table_head_3 = new PdfPCell(new Paragraph("Amount"));
        PdfPCell table_head_4 = new PdfPCell(new Paragraph("Type"));
        table_head_1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table_head_2.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table_head_3.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table_head_4.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell(table_head_1);
        table.addCell(table_head_2);
        table.addCell(table_head_3);
        table.addCell(table_head_4);


        /*---------db-------------*/

       /*---------db-------------*/
        dbHelper = new DbHelper(getContext());
        sqLiteDatabase = dbHelper.getReadableDatabase();
        if (!print_date_from.equals("")) {
            if (print_date_to.equals("")) {
                print_date_from = "";
                print_date_to = "";
            }
        }
        Cursor cursor_payment = dbHelper.getPayment(sqLiteDatabase,spStore.getLoginID(), print_account, print_date_from, print_date_to);

        Cursor account_cursor;
        String payment_id, account_id, account_name, payent_amount, account_type, payment_date;
        if (cursor_payment.moveToFirst()) {

            do {

                payment_id = cursor_payment.getString(0);
                account_id = cursor_payment.getString(2);
                account_cursor = dbHelper.getAccount(sqLiteDatabase, account_id,spStore.getLoginID());
                if (account_cursor.moveToFirst()) {
                    account_name = account_cursor.getString(2);
                } else {
                    account_name = "";
                }

                account_type = cursor_payment.getString(3);
                payent_amount = cursor_payment.getString(4);
                payment_date = cursor_payment.getString(6);

                PdfPCell table_col1 = new PdfPCell(new Paragraph(payment_date));
                PdfPCell table_col2 = new PdfPCell(new Paragraph(account_name));
                PdfPCell table_col3 = new PdfPCell(new Paragraph(payent_amount));
                PdfPCell table_col4 = new PdfPCell(new Paragraph(account_type));

                table.addCell(table_col1);
                table.addCell(table_col2);
                table.addCell(table_col3);
                table.addCell(table_col4);
            } while (cursor_payment.moveToNext());

        } else {
            // todo contact list empty

        }

 document.add(table);
        //Step 5: Close the document
        document.close();
        viewPdf();
    }

    private void viewPdf() {
        Alerts.progressDialogClose();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }


    protected class BackTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            Alerts.progressDialog(getActivity(), "Plase Wait.....");
        }

        @Override
        protected String doInBackground(String... params) {
            String id = spStore.getLoginID();
            return WebService.changePassword(id, params[0], params[1]);
        }

        @Override
        protected void onPostExecute(String response) {
            Alerts.progressDialogClose();
            pass_change_response = response.trim();

            if (!pass_change_response.equals("error")) {
                Log.e("-->", pass_change_response);
                if (pass_change_response.equals("success")) {
                    Toast.makeText(getContext(), "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                    spStore.removeLoginStatus();
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            } else {

            }


        }
    }
}
